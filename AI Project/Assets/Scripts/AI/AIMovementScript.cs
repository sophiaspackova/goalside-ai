﻿using UnityEngine;
using System.Collections;

public class AIMovementScript : MonoBehaviour {

	//Goal-side AI

	public GameObject player;
	public GameObject target;
	public float radius = 30;
	public float minRadius = 20;
	public float radiusToTarget = .1f;
	public float speed = .2f;
	public bool justStarted;
	public float waitTime;
	float currWaitTime;
	// Use this for initialization
	void Start () {
	
		justStarted = true;
		currWaitTime = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 distance = target.transform.position - gameObject.transform.position;
		Vector3 distancePlayer = target.transform.position - player.transform.position;
		float dotProduct = Vector3.Dot (gameObject.transform.position, player.transform.position);

		if (distance.magnitude > distancePlayer.magnitude || dotProduct > 0) {
			//catch up time
			Vector3 playerToAI = player.transform.position - gameObject.transform.position;
			float actualRadius = playerToAI.magnitude;
			Debug.Log (distance.magnitude);
			if (distance.magnitude >= radiusToTarget) {
				if (actualRadius > radius) {
					if (justStarted && currWaitTime < waitTime) {
						currWaitTime += Time.deltaTime;
						if (currWaitTime >= waitTime) {
							justStarted = false;
						}

					} else {
						currWaitTime = 0f;
						Vector3 position = gameObject.transform.position;
						distance = distance.normalized;
						distance *= speed;
						position.x += distance.x;
						position.y += distance.y;
						position.z += distance.z;
						Debug.Log ("Moving to Target");
						gameObject.transform.position = position;
					}
				} else if (actualRadius < minRadius) {
					if (justStarted && currWaitTime < waitTime) {
						currWaitTime += Time.deltaTime;
						if (currWaitTime >= waitTime) {
							justStarted = false;
						}

					} else {
						currWaitTime = 0f;
						Vector3 position = gameObject.transform.position;
						distancePlayer = distancePlayer.normalized;
						distancePlayer *= speed;
						position.x += distancePlayer.x;
						position.y += distancePlayer.y;
						position.z += distancePlayer.z;
						Debug.Log ("Moving to Player");
						gameObject.transform.position = position;
					}
				} else {
					justStarted = true;
				}
			} else {
				Debug.Log ("Too close");
			}
		}

	}
}
